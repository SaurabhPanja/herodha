from app import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import login

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True)
    email = db.Column(db.String(120),unique=True)
    password_hash = db.Column(db.String(128))
    current_holdings = db.relationship('BuyTransaction', backref='user', lazy=True)
    past_holdings = db.relationship('SellTransaction', backref='user', lazy=True)
    funds = db.Column(db.Float, default=100000.00)

    def __repr__(self):
        return '<User {}>'.format(self.username)
    
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
    
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

#transaction happens on every buy
class BuyTransaction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_name = db.Column(db.String(50))
    company_code = db.Column(db.String(10))
    qty = db.Column(db.Integer)
    last_price = db.Column(db.Float)
    Total = db.Column(db.Float)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return '<Transactions {}>'.format(self.company_name)

#transaction happens on every buy
class SellTransaction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_name = db.Column(db.String(50))
    company_code = db.Column(db.String(10))
    qty = db.Column(db.Integer)
    buying_price = db.Column(db.Float)
    selling_price = db.Column(db.Float)
    profit = db.Column(db.Float)
    total_buying = db.Column(db.Float)
    total_selling = db.Column(db.Float)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return '<Transactions {}>'.format(self.company_name)


class AllStocks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_symbol = db.Column(db.String(20), index=True)
    company_name = db.Column(db.String(100), index=True)

    def __repr__(self):
        print(self.company_name)

       
@login.user_loader
def load_user(id):
    return User.query.get(int(id))
from flask import Flask, render_template, request,flash, redirect, url_for, abort
from nsetools import Nse
from config import Config
from flask_sqlalchemy import SQLAlchemy
from app.forms import LoginForm
from flask_migrate import Migrate
from flask_login import current_user, login_user, logout_user
from app.models import User
from werkzeug.urls import url_parse
from app.forms import RegistrationForm
from flask_login import login_required
from app.forms import EditProfileForm
from app.models import *
import os
from elasticsearch import Elasticsearch

nse = Nse()
es = Elasticsearch('http://localhost:9200')

from app import app, db, celery

### testing
@app.route('/test_register')
def test_register():
    return render_template('test_register.html')

@app.route('/')
@app.route('/index')
@login_required
def index():
    #Market Action
    nifty_50 = nse.get_index_quote('nifty 50')
    nifty_bank = nse.get_index_quote('nifty bank')
    nifty_pharma = nse.get_index_quote('nifty pharma')
    nifty_it = nse.get_index_quote('nifty it')
    nifty_auto = nse.get_index_quote('nifty auto')

    #Top Gainers
    top_gainers = nse.get_top_gainers()

    #Top Losers
    top_losers = nse.get_top_losers()

    return render_template('index.html', nifty_50=nifty_50, nifty_auto=nifty_auto, nifty_bank=nifty_bank, nifty_it=nifty_it, nifty_pharma=nifty_pharma, top_gainers=top_gainers, top_losers=top_losers)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid Username or password')
            return render_template('login.html', form=form, error=True)
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', form=form)
    # return render_template('login.html', form=form)

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        print("Validating form")
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations you are now registered user!')
        login_user(user)
        return redirect('/')
    return render_template('register.html', form=form)
    # return render_template('register.html', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/dashboard')
@login_required
def profile():
    user = User.query.filter_by(username=current_user.username).first()
    return render_template('profile.html',user=user)

@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if request.method == "POST" and form.validate():
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    return render_template('edit_profile.html', form=form)


@app.route('/get_quote', methods=['POST'])
@login_required
def get_quote():
    company_code = request.form['stock']
    quote = nse.get_quote(company_code)
    if quote == None:
        return "Sorry Can't find it :("
    return render_template("get_quote.html", quote=quote)

@app.route('/buy_stock', methods=['POST'])
@login_required
def buy_stock():
    company_name = request.form['companyName']
    company_code = request.form['symbol']
    qty = request.form['qty']
    last_price = request.form['lastPrice']
    total = round(float(request.form['total']), 2)
    user = current_user
    fund = round(float(current_user.funds), 2)
    print("India")
    if fund < total:
        return "Garib, jake paise bhar apne account mai"
    else:
        current_user.funds = fund - total
    buy_transaction = BuyTransaction(company_name=company_name, company_code=company_code, qty=qty, last_price=last_price, Total=total, user=user)
    print(total, fund)
    db.session.add(current_user)
    db.session.add(buy_transaction)
    db.session.commit()
    return redirect(url_for('profile'))

@app.route('/sell_stock/<transaction_id>', methods=['POST'])
@login_required
def sell_stock(transaction_id):
    buy_transaction = BuyTransaction.query.get(transaction_id)
    quote = nse.get_quote(buy_transaction.company_code)
    SellingPrice = float(quote['lastPrice']) * float(buy_transaction.qty) 
    profit = float(SellingPrice) - float(buy_transaction.Total)
    profit = round(profit, 2)
    sell_transaction = SellTransaction(company_name=buy_transaction.company_name, company_code=buy_transaction.company_code, qty=buy_transaction.qty, buying_price=buy_transaction.last_price, selling_price=SellingPrice,profit=profit, total_buying=buy_transaction.Total, total_selling=buy_transaction.Total + profit, user=buy_transaction.user)
    current_user.funds += buy_transaction.Total + profit
    db.session.add(current_user)
    db.session.add(sell_transaction)
    db.session.delete(buy_transaction)
    db.session.commit()
    return redirect(url_for('profile'))

@app.route('/get_current_price/<company_code>')
def get_current_price(company_code):
    quote = nse.get_quote(company_code)
    return str(quote["lastPrice"])

#celery implementation
@app.route('/generate_file/<id>')
@login_required
def generate_file(id):
    task_id = create_log.delay(id)
    return {'id' : str(task_id)}

@celery.task(name="tasks.create_log")
def create_log(id):
    current_user = User.query.get(id)
    transactions = SellTransaction.query.filter_by(user=current_user)
    transactions_text = "Name : {}\n".format(current_user.username)
    for t in transactions:
        transactions_text += f'''
        Company_Name : {t.company_name} 
        Company_Code : {t.company_code}
        Quantity : {t.qty}
        Buying_Price : {t.buying_price}
        Selling_Price : {t.selling_price}
        Profit : {t.profit}
        Total_Buying : {t.total_buying}
        Total_Selling : {t.total_selling}
        ========================================'''

    file_path = os.path.join(os.getcwd(), "app", "static", "{}.txt".format(current_user.id))
    if os.path.exists(file_path):
        os.remove(file_path)

    with open(file_path, "w") as f: 
        f.write(transactions_text) 
        
    # return {"url" : "/static/{}.txt".format(current_user.id)}
    url = "/static/{}.txt".format(current_user.id)
    # file_location = "/static/{}.txt".format(current_user.id)
    return url



@app.route('/get_file/<id>')
def get_file(id):
    task = celery.AsyncResult(id)
    url = task.result
    return {'url' : url}

@app.route('/get_index')
def get_index():
    nifty_50 = nse.get_index_quote('nifty 50')
    nifty_bank = nse.get_index_quote('nifty bank')
    nifty_pharma = nse.get_index_quote('nifty pharma')
    nifty_it = nse.get_index_quote('nifty it')
    nifty_auto = nse.get_index_quote('nifty auto')


    print("Nifty 50")
    print("Last_price : {}".format(nifty_50['lastPrice']))
    print("Change : {}".format(nifty_50['change']))
    print("%Change : {}".format(nifty_50['pChange']))

    print("Nifty Bank")
    print("Last_price : {}".format(nifty_bank['lastPrice']))
    print("Change : {}".format(nifty_bank['change']))
    print("%Change : {}".format(nifty_bank['pChange']))

    print("Nifty Pharma")
    print("Last_price : {}".format(nifty_pharma['lastPrice']))
    print("Change : {}".format(nifty_pharma['change']))
    print("%Change : {}".format(nifty_pharma['pChange']))

    print("Nifty IT")
    print("Last_price : {}".format(nifty_it['lastPrice']))
    print("Change : {}".format(nifty_it['change']))
    print("%Change : {}".format(nifty_it['pChange']))

    print("Nifty Auto")
    print("Last_price : {}".format(nifty_auto['lastPrice']))
    print("Change : {}".format(nifty_auto['change']))
    print("%Change : {}".format(nifty_auto['pChange']))
    
    return "Check Console"

@app.route('/search_stocks', methods=['GET', 'POST'])
def search_stocks():
    stock = request.form['stock']
    result = es.search(index='stocks', body={'query' : {'match' :{'company_name' : stock}}})
    return result['hits']

@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500